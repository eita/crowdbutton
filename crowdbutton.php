<?php
/**
 * Plugin Name: CrowdButton
 * Version: 1.0.0
 * Plugin URI:
 * Description: Create custom buttons on your site to retrieve crowd source data
 * Author: Alan Tygel
 * Author URI:
 * Requires at least: 4.0
 * Tested up to: 4.0
 *
 * Text Domain: crowdbutton
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Alan Tygel
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Load plugin class files.
require_once 'includes/class-crowdbutton.php';
require_once 'includes/class-crowdbutton-settings.php';

// Load plugin libraries.
require_once 'includes/lib/class-crowdbutton-admin-api.php';
require_once 'includes/lib/class-crowdbutton-post-type.php';
require_once 'includes/lib/class-crowdbutton-taxonomy.php';
require_once 'includes/lib/crowdbutton-shortcode.php';
require_once 'includes/lib/crowdbutton-api.php';

/**
 * Returns the main instance of CrowdButton to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object CrowdButton
 */
function crowdbutton() {
	$instance = CrowdButton::instance( __FILE__, '1.0.0' );

	if ( is_null( $instance->settings ) ) {
		$instance->settings = CrowdButton_Settings::instance( $instance );
	}

	return $instance;
}

crowdbutton();
