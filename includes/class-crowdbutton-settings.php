<?php
/**
 * Settings class file.
 *
 * @package WordPress Plugin Template/Settings
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Settings class.
 */
class CrowdButton_Settings {

	/**
	 * The single instance of CrowdButton_Settings.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin settings.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available settings for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $settings = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;

		$this->base = 'crowdbutton_';

		// Initialise settings.
		add_action( 'init', array( $this, 'init_settings' ), 11 );

		// Register plugin settings.
		add_action( 'admin_init', array( $this, 'register_settings' ) );

		// Add settings page to menu.
		add_action( 'admin_menu', array( $this, 'add_menu_item' ) );

		// Add settings link to plugins page.
		add_filter(
			'plugin_action_links_' . plugin_basename( $this->parent->file ),
			array(
				$this,
				'add_settings_link',
			)
		);

		// Configure placement of plugin settings page. See readme for implementation.
		add_filter( $this->base . 'menu_settings', array( $this, 'configure_settings' ) );
	}

	/**
	 * Initialise settings
	 *
	 * @return void
	 */
	public function init_settings() {
		$this->settings = $this->settings_fields();
	}

	/**
	 * Add settings page to admin menu
	 *
	 * @return void
	 */
	public function add_menu_item() {

		$args = $this->menu_settings();

		// Do nothing if wrong location key is set.
		if ( is_array( $args ) && isset( $args['location'] ) && function_exists( 'add_' . $args['location'] . '_page' ) ) {
			switch ( $args['location'] ) {
				case 'options':
				case 'submenu':
					$page = add_submenu_page( $args['parent_slug'], $args['page_title'], $args['menu_title'], $args['capability'], $args['menu_slug'], $args['function'] );
					break;
				case 'menu':
					$page = add_menu_page( $args['page_title'], $args['menu_title'], $args['capability'], $args['menu_slug'], $args['function'], $args['icon_url'], $args['position'] );
					break;
				default:
					return;
			}
			add_action( 'admin_print_styles-' . $page, array( $this, 'settings_assets' ) );
		}
	}

	/**
	 * Prepare default settings page arguments
	 *
	 * @return mixed|void
	 */
	private function menu_settings() {
		return apply_filters(
			$this->base . 'menu_settings',
			array(
				'location'    => 'menu', // Possible settings: options, menu, submenu.
				'parent_slug' => 'options-general.php',
				'page_title'  => __( 'Relatório de cliques', 'crowdbutton' ),
				'menu_title'  => __( 'Relatório de cliques', 'crowdbutton' ),
				'capability'  => 'manage_options',
				'menu_slug'   => $this->parent->_token . '_settings',
				'function'    => array( $this, 'settings_page' ),
				'icon_url'    => '',
				'position'    => null,
			)
		);
	}

	/**
	 * Container for settings page arguments
	 *
	 * @param array $settings Settings array.
	 *
	 * @return array
	 */
	public function configure_settings( $settings = array() ) {
		return $settings;
	}

	/**
	 * Load settings JS & CSS
	 *
	 * @return void
	 */
	public function settings_assets() {

		// We're including the farbtastic script & styles here because they're needed for the colour picker
		// If you're not including a colour picker field then you can leave these calls out as well as the farbtastic dependency for the wpt-admin-js script below.
		wp_enqueue_style( 'farbtastic' );
		wp_enqueue_script( 'farbtastic' );

		// We're including the WP media scripts here because they're needed for the image upload field.
		// If you're not including an image upload then you can leave this function call out.
		wp_enqueue_media();

		wp_register_script( $this->parent->_token . '-settings-js', $this->parent->assets_url . 'js/settings' . $this->parent->script_suffix . '.js', array( 'farbtastic', 'jquery' ), '1.0.0', true );
		wp_enqueue_script( $this->parent->_token . '-settings-js' );
	}

	/**
	 * Add settings link to plugin list table
	 *
	 * @param  array $links Existing links.
	 * @return array        Modified links.
	 */
	public function add_settings_link( $links ) {
		$settings_link = '<a href="options-general.php?page=' . $this->parent->_token . '_settings">' . __( 'Configurações', 'crowdbutton' ) . '</a>';
		array_push( $links, $settings_link );
		return $links;
	}

	/**
	 * Build settings fields
	 *
	 * @return array Fields to be displayed on settings page
	 */
	private function settings_fields() {

		$settings['standard'] = array(
			'title'       => __( '', 'crowdbutton' ),
			'description' => __( '', 'crowdbutton' ),
		);

		$settings = apply_filters( $this->parent->_token . '_settings_fields', $settings );

		return $settings;
	}

	/**
	 * Register plugin settings
	 *
	 * @return void
	 */
	public function register_settings() {
		if ( is_array( $this->settings ) ) {

			// Check posted/selected tab.
			//phpcs:disable
			$current_section = '';
			if ( isset( $_POST['tab'] ) && $_POST['tab'] ) {
				$current_section = $_POST['tab'];
			} else {
				if ( isset( $_GET['tab'] ) && $_GET['tab'] ) {
					$current_section = $_GET['tab'];
				}
			}
			//phpcs:enable

			foreach ( $this->settings as $section => $data ) {

				if ( $current_section && $current_section !== $section ) {
					continue;
				}

				// Add section to page.
				add_settings_section( $section, $data['title'], array( $this, 'settings_section' ), $this->parent->_token . '_settings' );

				if ( ! $current_section ) {
					break;
				}
			}
		}
	}

	/**
	 * Settings section.
	 *
	 * @param array $section Array of section ids.
	 * @return void
	 */
	public function settings_section( $section ) {
		$html = '<p> ' . $this->settings[ $section['id'] ]['description'] . '</p>' . "\n";


	  $q = new WP_Query(array(
	    'post_type' => 'question',
	  ));

		if ( $q->have_posts() ) {
	      while ( $q->have_posts() ) {
	          $q->the_post();
	          $question = get_the_content();
	          $question_id = get_the_ID();
						$form = get_post_meta( $q->post->ID, 'form', true );

						$html .= "<div class='settings_question' id='q$question_id'>";

						$html .= "<h2>" . __( 'Pergunta: ', 'crowdbutton' ) . "$question</h2>";
						$html .= "<h3>" . __( 'Últimos cliques', 'crowdbutton' ) . "</h3>";

						global $wpdb;
						$results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}crowdbutton_clicks WHERE question_id = $question_id  ORDER BY time DESC LIMIT 10", OBJECT );

						$html .= "<table>";
						foreach ($results as $result) {
							$html .= "<tr>";
							$html .= "<td><a href='" . get_permalink( $result->object_id ) . "'>" . get_the_title( $result->object_id ) . "</a></td>";
							if($form == 1){
								$cols = explode('|', $result->data);
								foreach ($cols as $col) {
									$html .= "<td>" . $col . "</td>";
								}
							}
							$html .= "<td>" . strftime("%d/%m/%Y, às %H:%M", strtotime($result->time)) . "</td>";
							$html .= "<tr>";
						}
						$html .= "</table>";


						$html .= "<h3>" . __( 'Cliques no últimos 30 dias', 'crowdbutton' ) . "</h3>";

						$lastmonth = strftime("%Y-%m-%d %H:%M:%S", strtotime("-1 month"));
						$results = $wpdb->get_results( "SELECT object_id, count(*) as 'total' FROM {$wpdb->prefix}crowdbutton_clicks WHERE question_id = $question_id AND time > '$lastmonth' GROUP BY object_id ORDER BY total DESC", OBJECT );

						$html .= "<table>";
						foreach ($results as $result) {
							$html .= "<tr>";
							$html .= "<td><a href='" . get_permalink( $result->object_id ) . "'>" . get_the_title( $result->object_id ) . "</a></td>";
							$html .= "<td>" . $result->total . "</td>";
							$html .= "<tr>";
						}
						$html .= "</table>";

						$html .= "<h3>" . __( 'Total de cliques', 'crowdbutton' ) . "</h3>";

						$results = $wpdb->get_results( "SELECT object_id, count(*) as 'total' FROM {$wpdb->prefix}crowdbutton_clicks WHERE question_id = $question_id GROUP BY object_id  ORDER BY total DESC", OBJECT );

						$html .= "<table>";
						foreach ($results as $result) {
							$html .= "<tr>";
							$html .= "<td><a href='" . get_permalink( $result->object_id ) . "'>" . get_the_title( $result->object_id ) . "</a></td>";
							$html .= "<td>" . $result->total . "</td>";
							$html .= "<tr>";
						}
						$html .= "</table>";

						$html .= "<span class=\"csv_download\"><a href='?page=crowdbutton_settings&download_csv=$question_id'>Download CSV</a></span>";

						$html .= "</div>";

	      }
	      wp_reset_postdata();
	  } else {
	      return sprintf( __( 'Wrong id %s; question not found!', 'crowdbutton' ), $id);
	  }

		echo $html; //phpcs:ignore
	}

	public function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {
		header( 'Content-Type: application/csv' );
    header( 'Content-Disposition: attachment; filename="' . $filename . '";' );

    // clean output buffer
    ob_end_clean();

    $handle = fopen( 'php://output', 'w' );

		$delimiter = ";";
    // use keys as column titles
    fputcsv( $handle, array_keys( $array['0'] ), $delimiter );

    foreach ( $array as $value ) {
        fputcsv( $handle, $value , $delimiter );
    }

    fclose( $handle );

    // flush buffer
    ob_flush();

    // use exit to get rid of unexpected output afterward
    exit();
	}

	/**
	 * Load settings page content.
	 *
	 * @return void
	 */
	public function settings_page() {

		if ( isset( $_GET['download_csv'] ) && $_GET['download_csv'] ) {

			$question_id = $_GET['download_csv'];

			global $wpdb;
			$results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}crowdbutton_clicks WHERE question_id = $question_id  ORDER BY time DESC", OBJECT );

			$out = array();
			foreach ($results as $result) {
				$row = array(
					"Dia" => strftime("%d/%m/%Y", strtotime($result->time)),
					"Hora" => strftime("%H:%M", strtotime($result->time)),
					"Item" => get_the_title( $result->object_id ),
				);
				// add data
				$cols = explode('|', $result->data);
				foreach ($cols as $i => $col) {
					$row["Dados_" . $i] = $col;
				}
				$out[] = $row;
			}

			$this->array_to_csv_download(
				$out,
				"clicks.csv"
			);
			exit();
		}

		// Build page HTML.
		$html      = '<div class="wrap" id="' . $this->parent->_token . '_settings">' . "\n";
		$html .= '<h2>' . __( 'Relatório de cliques', 'crowdbutton' ) . '</h2>' . "\n";

		$tab = '';
		//phpcs:disable
		if ( isset( $_GET['tab'] ) && $_GET['tab'] ) {
			$tab .= $_GET['tab'];
		}
		//phpcs:enable

		// Show page tabs.
		if ( is_array( $this->settings ) && 1 < count( $this->settings ) ) {

			$html .= '<h2 class="nav-tab-wrapper">' . "\n";

			$c = 0;
			foreach ( $this->settings as $section => $data ) {

				// Set tab class.
				$class = 'nav-tab';
				if ( ! isset( $_GET['tab'] ) ) { //phpcs:ignore
					if ( 0 === $c ) {
						$class .= ' nav-tab-active';
					}
				} else {
					if ( isset( $_GET['tab'] ) && $section == $_GET['tab'] ) { //phpcs:ignore
						$class .= ' nav-tab-active';
					}
				}

				// Set tab link.
				$tab_link = add_query_arg( array( 'tab' => $section ) );
				if ( isset( $_GET['settings-updated'] ) ) { //phpcs:ignore
					$tab_link = remove_query_arg( 'settings-updated', $tab_link );
				}

				// Output tab.
				$html .= '<a href="' . $tab_link . '" class="' . esc_attr( $class ) . '">' . esc_html( $data['title'] ) . '</a>' . "\n";

				++$c;
			}

			$html .= '</h2>' . "\n";
		}


				// Get settings fields.
				ob_start();
				settings_fields( $this->parent->_token . '_settings' );
				do_settings_sections( $this->parent->_token . '_settings' );
				$html .= ob_get_clean();

		$html             .= '</div>' . "\n";

		echo $html; //phpcs:ignore
	}

	/**
	 * Main CrowdButton_Settings Instance
	 *
	 * Ensures only one instance of CrowdButton_Settings is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see CrowdButton()
	 * @param object $parent Object instance.
	 * @return object CrowdButton_Settings instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of CrowdButton_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of CrowdButton_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
