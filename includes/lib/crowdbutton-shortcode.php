<?php

// Defines plugin add_shortcode

function crowdbutton_shortcode( $atts ) {
  $id = $atts['id'];
  $object_id = get_the_ID();

  // get question title
  $q = new WP_Query(array(
    'post_type' => 'question',
    'meta_query' => array(
      array (
        'key' => 'id',
        'value' => $id,
        'compare' => '='
      )
    ),
  ));

  // $q->the_post();
  $p = get_post( $q->post->ID );
  $question = $p->post_content;
  $thankyou_message = get_post_meta( $q->post->ID, 'thankyou_message', true );
  $question_id = $q->post->ID;
  $form = get_post_meta( $q->post->ID, 'form', true );
  $count = get_post_meta( $q->post->ID, 'count', true );
  $icon_class = get_post_meta( $q->post->ID, 'icon_class', true );

  if ($icon_class){
    $icon = "<i class='$icon_class' aria-hidden='true'></i>";
  } else {
    $icon = NULL;
  }

  if (!$thankyou_message){
    $thankyou_message = __("Thank you for your answer!", 'crowdbutton');
  }

  if($count == 1){
    global $wpdb;
  	$results = $wpdb->get_results( "SELECT count(*) as 'total' FROM {$wpdb->prefix}crowdbutton_clicks WHERE question_id = $question_id AND object_id = $object_id", OBJECT );
    $count = $results[0]->total;
    if($count > 0){
      $question .= " (<span class='count'>$count</span>)";
    }
  }

  $thankyou_box = "
    <div id=\"thankyou_$id\" class=\"crowdbutton crowdbutton_thankyou\">
      $thankyou_message
    </div>
  ";

  $class = "crowdbutton";
  $form_modal = "";
  if ($form == 1) {
    $form_modal = "
      <div id=\"myModal\" class=\"Modal is-hidden is-visuallyHidden\">
        <!-- Modal content -->
        <div id=\"crowdform_$id\" class=\"Modal-content\">
          <span id=\"closeModal\" class=\"Close\">&times;</span>
          <span>Descreva o dia e horário em que não encontrou a feira: </span><input id='crowdform_desc' type='text'><br>
          <span>Local procurado:</span><input id='crowdform_local' type='text'><br>
          <span>Sabe o novo local/horário?</span><input id='crowdform_novo' type='text'><br>
          <span>Seu email:</span><input id='crowdform_email' type='text'>
          <input type='submit' value='Enviar' class=\"form_crowdform\" qid=$id question_id=$question_id object_id=$object_id>
        </div>
      </div>
    ";
    $class = " crowdbutton-form";
  }

  $button = "
  <div>

  <button id=\"crowdbutton_$id\" class=\"$class\" qid=$id question_id=$question_id object_id=$object_id count=$count>
    $icon
    $question
  </button>
    $thankyou_box
    $form_modal
  </div>
  ";

  return $button;
}
