<?php

// Defines crowdbutton API

function crowdbutton_api() {
  register_rest_route( 'crowdbutton/v1', '/save_click', array(
    'methods' => 'POST',
    'callback' => 'save_click',
  ) );
}

function save_click( $data ) {

  global $wpdb;
  $table_name = $wpdb->prefix . 'crowdbutton_clicks';

  $question_id = $data['question_id'];
  $object_id = $data['object_id'];
  $data = $data['data'];

  $wpdb->query(
    "INSERT INTO $table_name (`time`, `object_id`, `question_id`, `data`) VALUES (
      CURRENT_TIME(),
      $object_id,
      $question_id,
      \"$data\"
    );"
  );

  return True;
}
