/**
 * Plugin Template frontend js.
 *
 *  @package WordPress Plugin Template/JS
 */


jQuery(document).ready(function() {
	var runAddCrowdButtonEvent = function () {
		jQuery('.crowdbutton').click(function () {
			jQuery(this).fadeOut(500);

			var question_id = jQuery(this)[0].attributes['question_id'].value
			var object_id = jQuery(this)[0].attributes['object_id'].value
			var count = jQuery(this)[0].attributes['count'].value

			jQuery.ajax({
			  method: "POST",
			  url: "/wp-json/crowdbutton/v1/save_click",
			  data: { question_id: question_id, object_id: object_id,data: null }
			})
			  .done(function( msg ) {
			    console.log( "Data Saved: " + msg );
			  });

			jQuery(this).fadeIn(500);
			jQuery('span', this).text(Number(count)+1);
			jQuery(this).prop("disabled",true);
			jQuery(this).addClass('disabled');
			// var thank_you_id = "#thankyou_" + jQuery(this)[0].attributes['qid'].value
			// jQuery(thank_you_id).fadeIn(500);
			// jQuery(thank_you_id).fadeOut(2000);
		});
	};
	if (typeof idecFeirasThemeItemDomLoadedHook !== 'undefined') {
		idecFeirasThemeItemDomLoadedHook.push(runAddCrowdButtonEvent);
	}
	runAddCrowdButtonEvent();

	var runAddCrowdButtonFormEvent = function () {
		jQuery('.crowdbutton-form').click(function () {
			// jQuery(this).fadeOut(500);
			// open form modal
			// jQuery('.form_modal').fadeIn(500);

			var modal = document.getElementById('myModal');
			// var container = jQuery('.myContainer');

			modal.className = "Modal is-visuallyHidden";
	    setTimeout(function() {
	      // container.className = "MainContainer is-blurred";
	      modal.className = "Modal";
	    }, 100);
	    // container.parentElement.className = "ModalOpen";

			// Get the close button
			var btnClose = document.getElementById("closeModal");
			// Close the modal
			btnClose.onclick = function() {
			    modal.className = "Modal is-hidden is-visuallyHidden";
			    body.className = "";
			    container.className = "MainContainer";
			    container.parentElement.className = "";
			}

			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
			    if (event.target == modal) {
			        modal.className = "Modal is-hidden";
			        body.className = "";
			        container.className = "MainContainer";
			        container.parentElement.className = "";
			    }
			}


		});
	};
	if (typeof idecFeirasThemeItemDomLoadedHook !== 'undefined') {
		idecFeirasThemeItemDomLoadedHook.push(runAddCrowdButtonFormEvent);
	}
	runAddCrowdButtonFormEvent();

	var runAddCrowdFormEvent = function () {
		jQuery('.form_crowdform').click(function () {

			var question_id = jQuery(this)[0].attributes['question_id'].value
			var object_id = jQuery(this)[0].attributes['object_id'].value
			var qid = jQuery(this)[0].attributes['qid'].value
			var data_desc = jQuery('#crowdform_desc')[0].value
			var data_email = jQuery('#crowdform_email')[0].value
			var data_local = jQuery('#crowdform_local')[0].value
			var data_novo = jQuery('#crowdform_novo')[0].value
			var data = data_desc + "|" + data_email + "|" + data_local + "|" + data_novo

			jQuery.ajax({
			  method: "POST",
			  url: "/wp-json/crowdbutton/v1/save_click",
			  data: { question_id: question_id, object_id: object_id, data: data }
			})
			  .done(function( msg ) {
			    console.log( "Data Saved: " + msg );
			  });

			jQuery('#myModal').fadeOut(500);
			var thank_you_id = "#thankyou_" + jQuery(this)[0].attributes['qid'].value
			jQuery(".crowdbutton-form").fadeOut(200);
			jQuery(thank_you_id).fadeIn(500);
			jQuery(thank_you_id).fadeOut(2000);


		});
	};
	if (typeof idecFeirasThemeItemDomLoadedHook !== 'undefined') {
		idecFeirasThemeItemDomLoadedHook.push(runAddCrowdFormEvent);
	}
	runAddCrowdFormEvent();


});
