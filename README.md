= Crowdbutton =

This wordpress plugin offers the possibility to add buttons to collect crwodsourcing information anywhere in your wordpress website. The plugin works through questions. Each time someone clicks the button, the following information are recorded: date/time, question, object id, additional data.

== Features ==

* Create unlimited number of buttons
* Set the text of button, a thank you message (after click)
* Possibility of showing the number of clicks
* Possibility of opening a form to send additional data
* Is is also possible to export a list of clicks for each question